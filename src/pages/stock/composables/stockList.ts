import { Ref, ref, unref, watch } from 'vue'
import axios from 'axios'
import { watchIgnorable } from '@vueuse/core'

type Filters = {
    isActive: boolean
    search: string
    category_name: string
}

type Pagination = {
    page: number
    perPage: number
    total: number
}

type Sorting = {
    sortBy: string
    sortingOrder: 'asc' | 'desc' | null
}

const makePaginationRef = () => ref<Pagination>({ page: 1, perPage: 10, total: 0 })
const makeFiltersRef = () => ref<Partial<Filters>>({ isActive: true, search: '', category_name: '' })
const makeSortingRef = () => ref<Sorting>({ sortBy: "", sortingOrder: null })


export const stocklist = (options?: {
    pagination?: Ref<Pagination>
    sorting?: Ref<Sorting>
    filters?: Ref<Partial<Filters>>
    condition?: []
}) => {
  const isLoading = ref(false)
  const dataList =  ref([]);
  const condition = ref([])

  const stockdetail = ref([
    { date: '日期' },
    { date: '2024-08-02', open: 10, close: 12, low: 9, high: 13 },
    { date: '2024-08-03', open: 10, close: 12, low: 9, high: 13 },
    { date: '2024-08-04', open: 10, close: 12, low: 9, high: 13 },
    { date: '2024-08-05', open: 10, close: 12, low: 9, high: 13 },
    { date: '2024-08-06', open: 10, close: 12, low: 9, high: 13 },
    { date: '2024-08-07', open: 10, close: 12, low: 9, high: 13 },
    { date: '2024-08-07', open: 10, close: 12, low: 9, high: 13 },
    { date: '2024-08-08', open: 10, close: 12, low: 9, high: 13 },
    { date: '2024-08-10', open: 12, close: 14, low: 11, high: 15 },
    // 更多数据...
  ])

  const { filters = makeFiltersRef(), sorting = makeSortingRef(), pagination = makePaginationRef() } = options || {}

  const fetch = async () => {
    isLoading.value = true

    // await sleep(1000)
    console.log("开始查询--::", condition.value)

     // 请求http 获取数据
    try {
        // Optional configuration object
        const config = {
            headers: {
            'Content-Type': 'application/json', // Specify content type if needed
            'Authorization': 'Bearer your-token' // Example of adding authorization header
            }
        };

        let data = {
            "orderBy": sorting.value.sortBy,
            "orderDirection": sorting.value.sortingOrder,
            "condition": condition.value,
            "searchKey": filters.value.search,
            "categoryName": filters.value.category_name,
        }

        const response = await axios.post('http://localhost:9527/decs-mst/api/list', data);
        console.log("response::", response.data.data)
        // filteredUsers = response.data.data
        dataList.value = response.data.data
    } catch (error) {
        dataList.value = []
        console.error('Error fetching users:', error);
    }

    isLoading.value = false
  }

  const { ignoreUpdates } = watchIgnorable([sorting, condition], fetch, { deep: true })
  
//   watch(
//     filters,
//     () => {
//       // Reset pagination to first page when filters changed
//       pagination.value.page = 1
//       fetch()
//     },
//     { deep: true },
//   )

  fetch()

  return {
    stockdetail,
    condition,
    dataList,
    isLoading,
    filters,
    sorting,
    pagination,
    fetch,
  }
}
